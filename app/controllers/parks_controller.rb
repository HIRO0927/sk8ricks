class ParksController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  
  def index
    @parks = Park.all.order(created_at: :desc)
  end
  
  def show
    @park = Park.find_by(id: params[:id])
  end
  
  def new
    @park = Park.new
  end
  
  def create
    @park = Park.new(content: params[:content], image_name: "default_park.jpg") # フォームから送信されたデータを受け取り、保存する処理
    if params[:image]
      @park.image_name = "#{@park.id}.jpg"
      image = params[:image]
      File.binwrite("public/park_images/#{@park.image_name}", image.read)
    end
    if @park.save
      flash[:notice] = t('new_park')
      redirect_to("/parks/index")
    else
      render("parks/new")
    end
  end
  
  def edit
    @park = Park.find_by(id: params[:id])
  end
  
  def update
    @park = Park.find_by(id: params[:id]) #元のデータを探して＠postに定義しろ
    @park.content = params[:content]      #元データを変更しろ
    if params[:image]
      @park.image_name = "#{@park.id}.jpg"
      image = params[:image]
      File.binwrite("public/park_images/#{@park.image_name}", image.read)
    end
    if @park.save                         #保存しろ
      flash[:notice] = t('updated')
      redirect_to("/parks/index")           #一覧に戻れ
    else
      render("parks/edit")
    end
  end
  
  def destroy
    @park = Park.find_by(id: params[:id]) #元のデータを探して＠postに定義しろ
    @park.destroy                         #元データを削除しろ
    #保存しろはいらない。ないから @post.save
    flash[:notice] = t('deleted')
    redirect_to("/parks/index")
  end
end
