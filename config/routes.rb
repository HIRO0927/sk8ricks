Rails.application.routes.draw do
  
  get '/parks/index' => 'parks#index'
  get '/parks/:id' => 'parks#show'
  post '/parks/create' => 'parks#create'
  get '/parks/:id/edit' => 'parks#edit'
  post '/parks/:id/update' => 'parks#update'
  post '/parks/:id/destroy' => 'parks#destroy'
  
  root   'static_pages#home'
  get    '/help',    to: 'static_pages#help'
  get    '/about',   to: 'static_pages#about'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
#  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  
  resources :microposts do
    resources :likes, only: [:create, :destroy]
  end
  
  scope '(:locale)', locale: /#{I18n.available_locales.map(&:to_s).join('|')}/ do
    resources :posts, param: :slug
  end
end
