class CreateParks < ActiveRecord::Migration[5.1]
  def change
    create_table :parks do |t|
      t.text :content

      t.timestamps
    end
  end
end
