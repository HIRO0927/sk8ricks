class AddImageNameToParks < ActiveRecord::Migration[5.1]
  def change
    add_column :parks, :image_name, :string
  end
end
